# -*- coding: utf-8 -*-
"""
Created on Wed May  5 11:14:01 2021

@author: Marc

Construit une matrice de confusion à partir de deux dossier de label
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import argparse
import glob
import os
from sklearn.metrics import confusion_matrix, cohen_kappa_score, f1_score, \
                            precision_score, recall_score, accuracy_score
import tqdm
from collections import defaultdict

def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = [0,0,0,0]
    y[0] = x[0] - x[2] / 2  # top left x
    y[1] = x[1] - x[3] / 2  # top left y
    y[2] = x[0] + x[2] / 2  # bottom right x
    y[3] = x[1] + x[3] / 2  # bottom right y
    xy = ['x1','y1','x2','y2']
    res = {i:j for i,j in zip(xy, y)}
    return res


def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    assert bb1['x1'] < bb1['x2']
    assert bb1['y1'] < bb1['y2']
    assert bb2['x1'] < bb2['x2']
    assert bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou
            

def read_dir(dir_labels, dir_pred, nb_classe = 1, iou_thres=0.5):
    '''
    Le but est de créer 2 vecteurs de tailles égale pour ensuite utiliser la
    fonction  confusion_matrix de sklearn.metrics
    nb_classe 1 ou 6 pour ce code:
        nombre de classe pour la matrice de confusion
    '''
    # parcours des dossier
    cwd = os.getcwd()
    
    res_label = []
    res_pred = []
    images_vide = 0    
    list_pred = glob.glob(f"{dir_pred}/*.txt")
    list_labels = glob.glob(f"{dir_labels}/*.txt")
    list_names_pred = [os.path.basename(x) for x in list_pred]
    list_names_labels = [os.path.basename(x) for x in list_labels]
    
    nb_individus = defaultdict(int)
    nb_predictions = defaultdict(int)
    
    # Pour chaque label détermine si il y a un TP, FP, FN ...
    for txt in tqdm.tqdm(list_names_labels): 
        
        file = os.path.join(cwd, dir_labels, txt)
        label = pd.read_csv(file, sep=' ', header=None, 
                                names=["espece", "x", "y", "width", "height"])      
        if label.empty is True:
                continue # si pas de GT on passe, on regarde pas les TN ici
        if txt in list_names_pred:
            file = os.path.join(cwd, dir_pred, txt)
            pred = pd.read_csv(file, sep=' ', header=None, 
                                names=["espece", "x", "y", "width", "height"])            
            
        else: # si il n'y a pas de prédiction -> FN              
            res_pred.extend([nb_classe]*len(label))
            nb_predictions[nb_classe] += len(label)
            images_vide += 1
            if nb_classe == 1:
                res_label.extend([0]*len(label))
                nb_individus[0] += len(label)
            else:
                res_label.extend(label['espece'])
                for i in label['espece']:
                    nb_individus[i] += 1
            continue  
             
        # pour suivre le nombre total de prédiction
        if nb_classe == 1:
            nb_predictions[0] += len(pred)
            nb_individus[0] += len(label)
        else:
            for i in pred['espece']:            
                nb_predictions[i] += 1
            for i in label['espece']:
                nb_individus[i] += 1
            
        # calculs des iou entres chques détections et labels 
        matrice = np.zeros((len(label),len(pred)))
        for i,j in enumerate(label.values):
            for k,l in enumerate(pred.values):
                matrice[i][k] = get_iou(xywh2xyxy(j[1:5]), xywh2xyxy(l[1:5]))
     

        size = min(len(label),len(pred))
        argpreds = []
        arglabels = []
        for i in range(size):
            # cherche l'IOU le plus grand
            argmax = matrice.argmax()
            argpred = argmax % len(pred)
            arglabel = argmax // len(pred)
            max_iou = matrice[arglabel, argpred]
            p = pred.iloc[argpred]
            j = label.iloc[arglabel]
            
            argpreds.append(argpred)
            arglabels.append(arglabel)
            # si l'IOU est inférieur au seuil -> FP et FN
            if max_iou < iou_thres:
                res_pred.append(nb_classe)
                if nb_classe == 1:
                    res_label.append(0)
                else:
                    res_label.append(j[0])
                    
                res_label.append(nb_classe)
                if nb_classe == 1:
                    res_pred.append(0)
                else:        
                    res_pred.append(p[0])    
        
            else:          
                if nb_classe == 1:
                    res_pred.append(0)
                    res_label.append(0)
                else:
                    res_pred.append(p[0])
                    res_label.append(j[0])
                    
            matrice[:,argpred] = matrice[:,argpred]-1        
            matrice[arglabel,:] = matrice[arglabel,:]-1 
        
        # si il y a plus de label -> FN
        if len(label)>len(pred):
            for i in range(len(label)):
                if i not in arglabels:
                    j = label.iloc[i]
                    res_pred.append(nb_classe)
                    if nb_classe == 1:
                        res_label.append(0)
                    else:
                        res_label.append(j[0])
        # si il y a plus de prediction -> FP               
        if len(label)<len(pred):
            for i in range(len(pred)):
                if i not in argpreds:
                    j = pred.iloc[i]
                    res_label.append(nb_classe)
                    if nb_classe == 1:
                        res_pred.append(0)
                    else:
                        res_pred.append(j[0])
            
    return res_label, res_pred, nb_individus, nb_predictions, images_vide

 

def main(args):
    label, pred, nb_i, nb_p, images_vide = read_dir(args.labels, args.pred, args.nb_classe, args.iou_thres)
    
    # Dataviz
    if args.mode:   
        mat_conf = confusion_matrix(label, pred,normalize = 'true')
        
    else:
        mat_conf = confusion_matrix(label, pred)
    
    if args.nb_classe == 1:
        especes = ['Poisson', 'Vide']
    else:
        especes = ['Alose', 'Anguille', 'Saumon', 'Silure', 'Lamproie', 'Autre', 'Vide']
    if args.plot:
        plt.figure(figsize=(9,6))
        ax = plt.axes()
        
        sns.heatmap(mat_conf, annot=True, #vmin=0, vmax=1,
                    cmap = sns.color_palette("Blues", as_cmap=True),
                    xticklabels = especes, 
                    yticklabels = especes,
                    robust = True,
                    ax=ax,
                    fmt='.0f'
                    )
        plt.setp(ax.get_yticklabels(), rotation=0, ha="right",
         rotation_mode="anchor")
        ax.set_xlabel('Prédiction')
        ax.set_ylabel('Vérité')
        plt.show()
    else:
        print('\n')
        print(mat_conf)
        print('\n')
    
    F1 = f1_score(label, pred, average='macro')
    K = cohen_kappa_score(label, pred)
    precision = precision_score(label, pred, average='macro')
    recall = recall_score(label, pred, average='macro')
    acc = accuracy_score(label, pred)
    
    if args.nb_classe == 1:
        especes = {0:'FISH',
                   1: "VIDE"}
        
    else:
        especes = {0:'ALO', 
                   1:'ANG',
                   2:'SAT', 
                   3:'SIL', 
                   4:'LMP',
                   5:'OTH', 
                   6:'EMP'}
    print('\n')
    print('-----Individus | Predictions----')
    for i in range(0,args.nb_classe+1):
        print(f"{especes[i]}    {nb_i[i]}         {nb_p[i]}")
    print('\n')
    print(f"Nombre de faux négatif (images) : {images_vide}")
    print('\n')
    print(f'precsion : {precision:.3f}')
    print(f'recall : {recall:.3f}')
    print(f'F1-score : {F1:.3f}')
    print(f'Kappa Cohen : {K:.3f}')
    print(f'accuracy : {acc:.3f}')
    somme=mat_conf.sum(0)
    print(somme)
    somme2 = mat_conf.sum(1)
    print(somme2)
    


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--labels', help='dossier des vrais labels', default='test_didson')
    parser.add_argument('--pred', help='dossier des labels prédits', default='arisdidson/multi_ad_t/labels')
    parser.add_argument('--iou_thres', help='seuil détection IOU', default=0.5, type=float)
    #parser.add_argument('--conf_thres', help='seuil détection confiance', default=0.2, type=float)
    parser.add_argument('--plot', help='afficher matrice confusion', action='store_false')
    parser.add_argument('--nb_classe', help='nombre de classe de poisson', default=6, type=int)
    parser.add_argument('--mode', help='normalise matrice confusion', action='store_true')
    args = parser.parse_args()
    main(args)
    #label, pred = read_dir(args.labels, args.pred, args.nb_classe, args.iou_thres)

