# Acoustique

This project is a Python pipeline to detect and class fish passages on DIDSON and ARIS cameras. It is based on modifications of the YOLOv5 CNN and data pretreatement. 
The projet is a fork of https://github.com/ultralytics/yolov5. 

We made available also two datasets to test the pipeline:
- [Training Dataset (TD)](https://zenodo.org/record/5046708): composed by a set of annotated images
- [Validation Dataset (VD)](https://zenodo.org/record/5092010): composed by a real-case scenario of videos


## Requirements for Windows, Linux and macOS

### Requirements for running the Python pipeline

A requirement.txt file is given in the main folder (see below). 

- **Python3.7**
- **PyTorch >= 1.7**
- **scikit_image >= 0.16**
- **opencv_python >= 4.5.1**
- **numpy >= 1.13**

Install then the required packages:

`pip install -r requirements.txt`

If you are not using a virtual environment, verify that the version of the command `pip` is set to `pip3`.

## How to test 

### How to test inference on an avi/mp4 video

You need to have your video on a folder

```bash
$ python pipeline_YOLOv5_NOfilter_v3.py --source path/to/dir --weights multiclasse_best.pt
 
```

## How to run inference
### Data preparation

The filenames must follow the following standard:
`NAMEOFTHECAMERA_YYYY-MM-DD_hhmmss.videoformat`
where:
- `NAMEOFTHECAMERA = ARIS or DIDSON`
- `YYYY-MM-DD = date`
- `hhmmss = timecode`
- `videoformat = .mp4 or .avi`

for example:

**ARIS_2017-08-09_230000.mp4**

**DIDSON_2013-11-04_031500.avi**


