#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 14:53:20 2021

@author: marc

Code pour le post traitement 
et le traking des individus ainsi que pour la création des csv de résultats
"""

import numpy as np
import pandas as pd
import os
import glob

from dataclasses import dataclass, field
from collections import Counter
import csv
import datetime



@dataclass
class Poisson:
    '''
    class correspondant à un poisson
    travail essentiellement sur les labels prédits et non les images
    associe chaque image cohérente ensemble
    extrait les caractéristiques (espèce, nombre d'images...)
    '''
    df_images: pd.DataFrame = field(repr=False, 
                                    default=pd.DataFrame(columns=["espece", 
                                                                  "x", "y", 
                                                                  "width", 
                                                                  "height", 
                                                                  "name"])
                                    ) # dataframe des labels d'un même poisson
    nb_images: int = field(init=False, default=0) # nombre d'image  
    img_path: str = field(repr=False, default='') # chemin vers les images
    espece: list = field(init=False, default_factory=list) # list des espèces prédites
    proba: list = field(init=False, default_factory=list) # probabilité d'appartenir à une espèce
    time_code: str = field(init=False, default='') # time code de la première apparition
    name: str = field(init=False, repr=False, default='') 
    reel_time: str = field(repr=False, default='') # temps réel de la première apparition (si date dans le nom de la vidéo)
    frame: int = field(default=0) # frame d'apparition
    direction: int = field(init=False, repr=False, default=0) # direction de nage prédite
    fps: int = field(repr=False, default=0) # fps de la vidéo
    date: date = field(init=False, repr=False, default='') # date
    
           
    def add_image(self, label):
        '''
        Ajoute le label au dataframe df_images
        '''
        self.df_images = self.df_images.append(label)
    
    def next_frame(self, label_2, seuil = 0.3, pred_frame=0, skip_limit=3):
        '''
        cherche si le prochain label est associé au même poisson
        ie: il y a un IOU supérieur au seuil entre les 2 box
        '''
        detection1 = self.df_images.iloc[-1]
        detection_suivante = np.zeros(len(label_2))
        # récupère les IOU du fichier label_2
        for k,l in enumerate(label_2.values):
            detection_suivante[k] = get_iou(xywh2xyxy(detection1[1:5]), xywh2xyxy(l[1:5]))
            
        argmax = detection_suivante.argmax() # le plus grand IOU
        cond3 = detection_suivante[argmax] > seuil # true si IOU > seuil
        
        frame = int(label_2.iloc[0]['name'].split('_')[-1]) # le numéro de frame
        cond2 = pred_frame + skip_limit + 1 >= frame # test si il n'y a pas trop de frame de passé entre 2 labels
        
        # si cond2 et cond3 ajoute le label au dataframe et incrémente le nb_image
        if cond3 and cond2:
            df = pd.DataFrame(label_2.iloc[argmax]).T
            self.add_image(df)
            self.nb_images += 1
            label_2.drop(argmax, inplace=True)
      
        return cond3 and cond2, label_2, frame
    
    def detection(self, list_image, skip_limit=3):  
        '''

        Parameters
        ----------
        list_image : list
            list de tous les labels prédit
        skip_limit : int, optional
            Nombre d'image max entre 2 détections. The default is 3.

        Returns
        -------
        list_skip : list
            list des labels qui n'ont pas été associé au poisson
        i : int
            indice du dernier label 
            
        Description
        -------
        Regarde les labels successif un par un et associe ensemble ceux qui sont cohérent
        s'arrète si il n'y a plus de label ou si la limite d'image pouvant être passé est atteinte

        '''
        nb_skip = 0
        list_skip = []
        for i,j in enumerate(list_image):
            # les labels sont soit des csv (la première fois qu'ils sont lu)
            # soit des dataframes (la seconde fois ils ont été stocké pour évité de les relire et pour les retirer 
            # les lignes déjà utilisées)
            if isinstance(j, str):
                label = pd.read_csv(j, sep=' ', header=None, 
                                     names=["espece", "x", "y", "width", "height"])
                label['name'] = [os.path.basename(j)[:-4]] * len(label)
            
            else:
                label = j
                
            label.set_index(np.arange(len(label)), inplace=True)
            
            # ajoute le premier label
            if i == 0:             
                self.add_image(label.iloc[i])
                pred_frame = int(label.iloc[0]['name'].split('_')[-1])
                if len(label) > 1:
                    label.drop(i, inplace=True)
                    list_skip.append(label)                
                continue 
            
            # ajoute le prochain label si necessaire
            suivante_ok, label_2, pred_frame = self.next_frame(label, seuil=0.2/(nb_skip+1), 
                                                   pred_frame=pred_frame,
                                                   skip_limit=skip_limit)
            # créer la list des labels skipé 
            if suivante_ok:
                nb_skip = 0
                if len(label) > 1:                   
                    list_skip.append(label_2)
            
            # si la limite d'image est dépassé, break la boucle for
            else:
                nb_skip += 1
                list_skip.append(label_2)
                if nb_skip == skip_limit+1:
                    break

        return list_skip, i
     
       
    def def_espece(self):
        '''
        tourne après le code détection (df_images est non vide)
        Détermine les espèces les plus associées à l'individus
        ainsi que le pourcentage d'apparition
        '''
        vote =  Counter(self.df_images['espece'])
        top_two = vote.most_common(3)
        list_espece = {0:'ALO',
                       1:'ANG',
                       2:'SAT',
                       3:'SIL',
                       4:'LPM',
                       5:'AUTRE'}
        for i in range(len(top_two)):
            self.espece.append(list_espece[top_two[i][0]])
            self.proba.append(top_two[i][1]/(self.nb_images+1))
        self.espece.extend(['',''])
        self.proba.extend([0,0])

      
    def make_name(self):
        '''
        Si le nom de la video est CAMERA_yyyy-mm-jj_hhmmss extrait le jour et l'heure de la vidéo
        
        '''
        self.name = self.df_images.iloc[0]['name']
        split_name = self.name.split('_')
        self.frame = int(split_name[-1])
        
        # self.fps = 8 #temporaire
        
        try : 
            self.time_code = datetime.timedelta(seconds=int(self.frame/self.fps))
            heure_brute = split_name[-3]
            start = datetime.timedelta(hours = int(heure_brute[:2]), 
                                 minutes = int(heure_brute[2:4]),
                                 seconds = int(heure_brute[4:6]))           
            self.reel_time = start + self.time_code
            self.date = split_name[1]
                                      
            
        except (ValueError, IndexError):
            print('Mauvais format de nom de vidéo')
     
        
    def out_list(self):   
        '''
        liste de sortie à imprimer dans le csv
        '''
        return [str(self.name), str(self.date),  str(self.reel_time), str(self.time_code),
                self.frame, self.nb_images, 
                self.espece[0], self.espece[1], self.espece[2], 
                f'{self.proba[0]:.2f}', f'{self.proba[1]:.2f}', 
                f'{self.proba[2]:.2f}', f'{self.direction:.2f}' 
                ]
       
    
    def calcul_direction(self):
        '''
        calcul de la direction du poisson
        ie: la moyenne des différences des images successives
        '''
        direction = []
        for i,j in enumerate(self.df_images.values):
            if i == 0:
                pre = j
            deplacement = j[1] - pre[1]
            direction.append(deplacement)
            pre = j
        self.direction = np.mean(direction)*100
    
    def save_position(self, path):
        '''
        enregistre les labels correspondants au même poisson dans un dossier séparé
        '''
        with open(os.path.join(path, str(self.name) + '.csv'), 'w') as file:
            writer = csv.writer(file)
            for i in self.df_images.values:              
                writer.writerow(i)
        
        
def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = [0,0,0,0]
    y[0] = x[0] - x[2] / 2  # top left x
    y[1] = x[1] - x[3] / 2  # top left y
    y[2] = x[0] + x[2] / 2  # bottom right x
    y[3] = x[1] + x[3] / 2  # bottom right y
    xy = ['x1','y1','x2','y2']
    res = {i:j for i,j in zip(xy, y)}
    return res


def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    assert bb1['x1'] < bb1['x2']
    assert bb1['y1'] < bb1['y2']
    assert bb2['x1'] < bb2['x2']
    assert bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou 


def analyse_1_video(dir_path, dir_image = '', video=True, 
                    skip_limit=3, min_image=5,
                    save_name=False, fps=10):
    '''
    analyse toute une vidéo (un dossier de label (fichier .txt))
    et écrit un csv avec une détection par ligne
    '''
    

    list_image = glob.glob(str(dir_path) + '/*.txt')
    print(str(dir_path))
    list_image.sort()
    
    # réarange l'ordre de list_image
    if video:     
        dict_image = {}
        for i in list_image:
            j = int(os.path.basename(i)[:-4].split('_')[-1])
            dict_image[j] = i
            
        list_sort = sorted(dict_image.items())
        list_image = []
        for i in list_sort:
            list_image.append(i[1])

        
    list_individus = []
    new_list = True
    
    # recherche tous les poissons différents dans les labels
    if list_image:
        while new_list:
            
            if isinstance(list_image[0], str):
                label = pd.read_csv(list_image[0], sep=' ', header=None, 
                                  names=["espece", "x", "y", "width", "height"])
            else:
                label=list_image[0]
                
            new_list = []
            for j in range(len(label)):
                P1 = Poisson(img_path=dir_image, fps=fps)
                new_list, i = P1.detection(list_image, skip_limit = skip_limit)
         
                list_individus.append(P1)
                list_image = new_list + list_image[i+1:]       
    
    list_poisson = []
    # élimine les poissons avec un nb_image < min_image
    for i in list_individus:
        if i.nb_images >= min_image:
            list_poisson.append(i)
            
            
    # sauvegarde des résultats dans un csv
    if not list_poisson:
        print('pas de poisson dans la vidéo')
    
    else:
        
        if not save_name:    
            try:
                save_name = str(dir_path)[:-6] + str(dir_path).split('/')[-2] + '.csv'
            except IndexError:
                save_name = str(dir_path)[:-6] + str(dir_path).split('\\')[-2] + '.csv'
            mode = 'w'
        else : 
            mode = 'a'
               
        with open(save_name, mode) as f:
            writer = csv.writer(f)
            if mode == 'w':
                row = ['name', 'date',  'temps_reel', 'time_code', '#frame', 'nb_frame', 
                        'espece1', 'espece2', 'espece3', 'probabilite1', 'probabilite2', 
                        'probabilite3', 'direction']
                writer.writerow(row)
            for i in list_poisson:
                i.def_espece()
                i.make_name()
                i.calcul_direction()
                print(i) 
                i.save_position(os.path.join(dir_path, '..', 'Poisson'))
                writer.writerow(i.out_list())

if __name__ == '__main__':
    # test
    # dir_vd = '/media/hersant_m/Nouveau nom/test_yolov5_VD1'
    # video_vd = os.listdir(dir_vd)
    # for i in video_vd:  
    #     os.mkdir(os.path.join(dir_vd, i, 'Poisson'))
    #     analyse_1_video(os.path.join(dir_vd, i, 'labels'),skip_limit=1, min_image=4)
    
    analyse_1_video('/home/hersant_m/Documents/Yolo/video_aris/ARIS_2017-10-25_003000/labels', 
                    skip_limit=2, min_image=5)
