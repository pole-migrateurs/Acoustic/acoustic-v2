# -*- coding: utf-8 -*-
"""
Created on Wed May 19 12:59:44 2021

@author: Marc
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import argparse
import glob
import os
from sklearn.metrics import confusion_matrix, cohen_kappa_score, f1_score, \
                            precision_score, recall_score, accuracy_score
import tqdm
from collections import defaultdict

def read_dir(dir_labels, dir_pred, nb_classe = 1):
    
    cwd = os.getcwd()
    
   
    list_pred = glob.glob(f"{dir_pred}/*.txt")
    list_labels = glob.glob(f"{dir_labels}/*.txt")
    list_names_pred = [os.path.basename(x) for x in list_pred]
    #list_names_labels = [os.path.basename(x) for x in list_labels]
    
    #nb_individus = defaultdict(int)
    nb_predictions = defaultdict(int)
    nb_empty = len(list_labels)
    nb_predictions[nb_classe] = nb_empty - len(list_pred)
    for txt in tqdm.tqdm(list_names_pred): 
        
        file = os.path.join(cwd, dir_pred, txt)
        pred = pd.read_csv(file, sep=' ', header=None, 
                                names=["espece", "x", "y", "width", "height"])
        for i in pred['espece']:
            nb_predictions[i] += 1
            
    return nb_predictions

def main(args):
    nb_i = read_dir(args.labels, args.pred, args.nb_classe)
    if args.nb_classe == 1:
        especes = {0:'FISH',
                   1: "VIDE"}
        
    else:
        especes = {0:'ALO', 
                   1:'ANG',
                   2:'SAT', 
                   3:'SIL', 
                   4:'LMP',
                   5:'FISH', 
                   6:'EMPTY'}
    print('\n')    
    for i in range(0,args.nb_classe+1):
        print(f"{especes[i]}    {nb_i[i]}")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--labels', help='dossier des vrais labels', default='empty_test')
    parser.add_argument('--pred', help='dossier des labels prédits', default='resultat_test/det/labels')
    parser.add_argument('--nb_classe', help='nombre de classe de poisson', default=6, type=int)    
    args = parser.parse_args()
    main(args)